import React, { Component } from 'react';
import Person from './Person/Person'
import './App.css'

class App extends Component {

  state = {
    persons : [
      { id : 'x1' , name: 'Max', age: 28 },
      { id : 'x2' , name: 'Manu', age: 29 },
      { id : 'x3' , name: 'Stephanie', age: 26 }
    ],

    personVisible : false
  }
  

  toggleHandler = () => {
    const doesShow = this.state.personVisible;
    this.setState( { personVisible : !doesShow } );
  }


  updateNameHandler = (event, id) => {

    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });

    const person = {
      ...this.state.persons[personIndex]
    };

    // const person = Object.assign({}, this.state.persons[personIndex]);
    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;
    this.setState( {persons: persons} );
    
  }


  deletePersonHandler = (personIndex) => {
    const persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({persons: persons});
  }


  // <Person data = {person} key={person.id} changed = {(event) => {this.updateNameHandler(event, person.id)}} />

  alertClass = [];

  getPersonsDomData = () => {
    
    const personsList = this.state.persons.map((person , index) => {
      
      const personData   = {};
      personData.person  = person;
      personData.changed = (event) => {this.updateNameHandler(event, person.id)};
      personData.click   = () => this.deletePersonHandler(index)
      personData.key     = person.id

      return <Person data = {personData} key={person.id} />

    })

    
    if(this.state.persons.length <=2){
      this.alertClass.push('red');
    }

    if(this.state.persons.length <=1){
      this.alertClass.push('bold');
    }

    
    return personsList;
  }


  render() {

    let personsRenderedList = null;
    if(this.state.personVisible === true){
      personsRenderedList = this.getPersonsDomData();
    }
    
    return (
      <div className="App">
        <h1>This is a Person App</h1>
        <p className = { this.alertClass.join(' ')}>The App is working find</p>
        <button onClick = {this.toggleHandler} className = 'btn-style' > Toggle </button>
        {personsRenderedList}
      </div>
    );

  }

}

export default App;
