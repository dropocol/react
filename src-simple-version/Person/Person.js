import React from 'react';
import './Person.css'

const person = (props) => {
    return (
        <div className = "Person" >
            <p onClick = {props.data.click}>My name is {props.data.person.name} and I am {props.data.person.age} years old</p>
            <input value = {props.data.person.name} onChange = {props.data.changed} />
        </div>
    )
}
/* */

export default person


