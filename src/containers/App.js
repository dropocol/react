import React, { Component } from 'react';
import Persons from '../components/Persons/Persons'
import Cockpit from '../components/Cockpit/Cockpit'

import classes from './App.css';

class App extends Component {

  state = {
    persons : [
      { id : 'x1' , name: 'Max', age: 28 },
      { id : 'x2' , name: 'Manu', age: 29 },
      { id : 'x3' , name: 'Stephanie', age: 26 }
    ],

    personVisible : false
  }
  

  toggleHandler = () => {
    const doesShow = this.state.personVisible;
    this.setState( { personVisible : !doesShow } );
  }


  updateNameHandler = (event, id) => {

    const personIndex = this.state.persons.findIndex(p => {
      return p.id === id;
    });

    const person = {
      ...this.state.persons[personIndex]
    };

    // const person = Object.assign({}, this.state.persons[personIndex]);
    person.name = event.target.value;
    const persons = [...this.state.persons];
    persons[personIndex] = person;
    this.setState( {persons: persons} );
    
  }


  deletePersonHandler = (personIndex) => {
    const persons = [...this.state.persons];
    persons.splice(personIndex, 1);
    this.setState({persons: persons});
  }


  // <Person data = {person} key={person.id} changed = {(event) => {this.updateNameHandler(event, person.id)}} />
  getPersonsDomData = () => {
    
    const personsList = < Persons
            persons={this.state.persons}
            clicked={this.deletePersonHandler}
            changed={this.updateNameHandler} />;
    return personsList;
  }


  render() {

    let personsRenderedList = null;
    if(this.state.personVisible === true){
      personsRenderedList = this.getPersonsDomData();
    }
    
    return (
      <div className={classes.App}>
        <Cockpit 
          personVisible = {this.state.personVisible}
          persons       = {this.state.persons}
          toggle        = {this.toggleHandler} />
        {personsRenderedList}
      </div>
    );

  }

}

export default App;
