import React from 'react';
import classes from './Cockpit.css';

const cockpit = (props) => {

    const assignedClasses = [];
    let btnClass = '';
    if (props.personVisible) {
        btnClass = classes.Red;
    }

    if ( props.persons.length <= 2 ) {
      assignedClasses.push( classes.red ); // classes = ['red']
    }
    if ( props.persons.length <= 1 ) {
      assignedClasses.push( classes.bold ); // classes = ['red', 'bold']
    }

    return (
        <div className={classes.Cockpit}>
            <h1>This is a Person App</h1>
            <p className = {assignedClasses.join(' ')}>The App is working find</p>
            <button onClick = {props.toggle}  className = {btnClass} > Toggle </button>
        </div>
    )
}
/* */

export default cockpit

