import React from 'react';
import classes from './Person.css';

const person = (props) => {
    return (
        <div className = {classes.Person} >
            <p onClick = {props.data.clicked}>My name is {props.data.person.name} and I am {props.data.person.age} years old</p>
            <input value = {props.data.person.name} onChange = {props.data.changed} />
        </div>
    )
}
/* */

export default person


