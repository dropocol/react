import React from 'react';

import Person from './Person/Person';

const persons = (props) => props.persons.map((person, index) => {

    const personData   = {};
    
    personData.person  = person;
    personData.clicked = () => props.clicked( index )
    personData.changed = (event) => props.changed( event, person.id );

    return <Person data = {personData} key={person.id} />


}); 


export default persons;